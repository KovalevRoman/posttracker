from django.urls import path, re_path

from rest_framework_jwt.views import obtain_jwt_token

from trackservice.accounts.api.views import UserCreateAPIView, UserLoginAPIView



urlpatterns = [
    path('register', UserCreateAPIView.as_view(), name='register'),
    path('login', UserLoginAPIView.as_view(), name='login'),
    path(r'auth', obtain_jwt_token),
]