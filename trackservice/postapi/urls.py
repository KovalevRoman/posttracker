from django.urls import path, re_path

from trackservice.postapi.views import TrackListAPIView, TrackDetailView, TrackUpdateView, TrackDeleteView, TrackCreateView

urlpatterns = [
    path('create', TrackCreateView.as_view(), name='create'),
    path('list', TrackListAPIView.as_view()),
    re_path('^tracks/(?P<pk>\d+)/$', TrackDetailView.as_view(), name='detail'),
    re_path('^tracks/(?P<pk>\d+)/edit/$', TrackUpdateView.as_view(), name='update'),
    re_path('^tracks/(?P<pk>\d+)/delete/$', TrackDeleteView.as_view(), name='delete'),
    # path('api/tracklist', TrackListApiView.as_view()),
]