from rest_framework.serializers import ModelSerializer
from trackservice.postapi.models import TrackModel, TrackStatusDetail


class TrackModelListSerializer(ModelSerializer):
    class Meta:
        model = TrackModel
        fields = [
            'owner',
            'track',
            'status',
            'description',
            'added',
            'notify_email',
            'notify_sms',
        ]


class TrackModelDetailSerializer(ModelSerializer):
    class Meta:
        model = TrackModel
        fields = [
            'owner',
            'track',
            'status',
            'description',
            'added',
            'notify_email',
            'notify_sms',
        ]

class TrackCreateSerializer(ModelSerializer):
    class Meta:
        model = TrackModel
        fields = [
            'owner',
            'track',
            # 'status',
            'description',
            # 'added',
            # 'notify_email',
            # 'notify_sms',
        ]