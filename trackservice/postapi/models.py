from django.db import models
# from django.conf import settings
from django.urls import reverse
from django.db.models import CASCADE
from django.contrib.auth.models import User

from datetime import datetime

from config import settings

# User = settings.AUTH_USER_MODEL
# class UserProfile(models.Model):
#     user = models.OneToOneField(User, on_delete=CASCADE)
#     email = models.EmailField(blank=True, max_length=30)
#     phone = models.CharField(max_length=30)
#     telegramm = models.CharField(max_length=30)
#     first_name = models.CharField(max_length=30)
#     second_name = models.CharField(max_length=30)


class TrackModel(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE, default=1)
    track = models.CharField(max_length=20, blank=False, unique=True)
    status = models.CharField(max_length=200, blank=False, default="Not processed")
    description = models.CharField(max_length=200, blank=True)
    added = models.DateTimeField(default=datetime.now)
    notify_email = models.BooleanField(default=False)
    notify_sms = models.BooleanField(default=False)

    def __str__(self):
        return self.track
        # return self.track + ' ' + str(self.added)

    def get_absolute_url(self):
        return reverse('detail', kwargs={'pk': self.pk})


class TrackStatusDetail(models.Model):
    track = models.ForeignKey(TrackModel, on_delete=CASCADE)
    date = models.DateTimeField(blank=True)
    status = models.CharField(max_length=50, blank=True, null=True)
    description = models.CharField(max_length=100, blank=True, null=True)
    weight = models.CharField(max_length=20, null=True)
    country = models.CharField(max_length=50, blank=True, null=True)
    city = models.CharField(max_length=50, blank=True, null=True)

    def __str__(self):
        return self.track.track + ' ' + self.date.strftime('%m/%d/%Y')
