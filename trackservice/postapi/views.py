from django.shortcuts import render
from django.db.models import Q
from rest_framework.generics import (
    ListAPIView,
    RetrieveAPIView,
    UpdateAPIView,
    DestroyAPIView,
    CreateAPIView,
    RetrieveUpdateAPIView
    )
from rest_framework.pagination import (
    LimitOffsetPagination,
    PageNumberPagination
)
from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
    IsAdminUser,
    IsAuthenticatedOrReadOnly,
    )
from .permissions import IsOwnerOrReadOnly
from trackservice.postapi.models import TrackModel, TrackStatusDetail
from trackservice.postapi.serializers import (
    TrackModelDetailSerializer,
    TrackModelListSerializer,
    TrackCreateSerializer
    )


class TrackListAPIView(ListAPIView):
    serializer_class = TrackModelListSerializer
    permission_classes = [IsOwnerOrReadOnly, IsAuthenticated]
    # pagination_class = LimitOffsetPagination #PageNumberPagination
    pagination_class = PageNumberPagination

    def get_queryset(self, *args, **kwargs):
        queryset = TrackModel.objects.filter(owner=self.request.user)
        return queryset


class TrackDetailView(RetrieveAPIView):
    queryset = TrackModel.objects.all()
    serializer_class = TrackModelDetailSerializer
    permission_classes = [IsOwnerOrReadOnly, IsAuthenticated]


class TrackUpdateView(RetrieveUpdateAPIView):
    queryset = TrackModel.objects.all()
    serializer_class = TrackModelDetailSerializer
    permission_classes = [IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly]

    def perform_update(self, serializer):
        serializer.save(owner=self.request.user)

class TrackDeleteView(DestroyAPIView):
    queryset = TrackModel.objects.all()
    serializer_class = TrackModelDetailSerializer



class TrackCreateView(CreateAPIView):
    queryset = TrackModel.objects.all()
    serializer_class = TrackCreateSerializer
    permission_classes = [IsAuthenticated]

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)