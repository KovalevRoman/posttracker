from django.contrib import admin

from trackservice.postapi.models import TrackModel, TrackStatusDetail

# Register your models here.
admin.site.register(TrackModel)
admin.site.register(TrackStatusDetail)